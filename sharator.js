;(function ($) {

  $(document).ready(function () {

    $('.sharator .soc-btn').click(function () {
      var type = $(this).data('media-name');
      var nid = $(this).data('nid');
      if (!$(this).hasClass('clicked')) {
        if($(this).find('.qty').length){
          var count = parseInt($('span.qty', $(this)).text());
          if (!isNaN(count)) {
            count = count + 1;
            $('.qty', $(this)).text(count);
          }
          $.ajax({
            type: 'POST',
            url: '/sharator/change/' + nid + '/' + type + '/' + count
          });
        }
        $(this).addClass('clicked').removeClass('empty');
        window.open($(this).attr("href"), 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');
      }
      return false;
    });

  });

})(jQuery);

Sharator module
---------------
Автор: Алексей Зубенко, alex-zubenko@yandex.ru


Описание
--------
Модуль позволяет выводить в нодах кнопки для шаринга в наиболее популярных социальных сетях.

В отличие от известных сервисов типа Share32 или Yandex.Share кнопки стилизуемы.
Для этого достаточно отключить дефолтную тему в настройках модуля и самому реализовать все свои представления о красоте.

Значения сохраняются в базе данных и доступны для просмотра в админке.
Для особо хитрых пользователей есть возможность переключения на «ручное управление» - значения вводятся вручную, а не из соцсетей

На данный момент поддерживается ВКонтакте, Facebook, Мой Мир, Одноклассники и Google+.
Twitter на момент написания модуля исключил из API возможность получать количество твитов. По крайней мере, сделать это в адекватные спроки не удалось.


Установка
---------
Устанавливается как обычно, далее в настройках включаются нужные типы материалов и социальные сети.
Страницы настроек: admin/config/services/sharator


Подробности
-----------
При отсутствии информации от социальной сети ссылке присваивается класс "no-data", количество репостов не выводится.
При нулевом количестве репостов ссылке присваивается класс "empty", репосты выведены, но скрыты
(при клике значение инкрементируется, количество репостов становится видимым).

html-код ссылки жестко регламентируется, все остальное может меняться переопределением файла sharator.tpl.php


P.S.
----
Модуль собран в свободное время на коленях, использовались методы, найденные на просторах интернета.


@todo: Сделать поле для views
@todo: Сделать возможность отключать вывод для тизера.
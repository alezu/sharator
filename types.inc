<?php

$types = array(
  array(
    'name' => 'ВКонтакте',
    'machine_name' => 'vk',
    'link' => 'http://vk.com/share.php?url=',
  ),
  array(
    'name' => 'Facebook',
    'machine_name' => 'fb',
    'link' => 'http://www.facebook.com/sharer.php?u=',
  ),
  array(
    'name' => 'Twitter',
    'machine_name' => 'tw',
    'link' => 'http://twitter.com/share?url=',
  ),
  array(
    'name' => 'Мой Мир',
    'machine_name' => 'mw',
    'link' => 'http://connect.mail.ru/share?share_url=',
  ),
  array(
    'name' => 'Одноклассники',
    'machine_name' => 'od',
    'link' => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=',
  ),
  array(
    'name' => 'Google+',
    'machine_name' => 'gp',
    'link' => 'https://plus.google.com/share?url=',
  ),
);
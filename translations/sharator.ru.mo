��          �      |      �     �     �  #     Y   8  �   �     !     ?      D     e     j     y     ~     �  	   �  &   �  C   �       "   '     J  b   b  "   �  �  �     �  .   �  n     �   }  �     I   �     >	  G   E	     �	  %   �	     �	  5   �	     
      
  N   8
  �   �
  ;     8   H  @   �  )  �  V   �                                                    	                                 
            Activity Configuration is saved... Configure the social share buttons. Don't forget to switch on needable buttons on the module <a href="@link">config page</a>. If you switch on this checkbox, you will be able to manually change reposts quantity for each available social media <a href="@link">here</a>. Manually set reposts quantity Name Node types available for sharing Page Reposts values Save Set default theme Settings Share in  Show reposts quantity on share buttons Simple module for inserting themable social share buttons to nodes. Social media integration Social share integration settings. Switch on share buttons This module is made especially for customizing social buttons but you can firstly use preset theme Viewing and editing reposts values Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2015-12-11 22:08+0400
PO-Revision-Date: 2015-12-11 21:11+0300
Last-Translator: Алексей Зубенко <alex-zubenko@yandex.ru>
Language-Team: Russian <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=((((n%10)==1)&&((n%100)!=11))?(0):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(1):2));
X-Generator: Poedit 1.5.4
 Активность Конфигурация сохранена... Настройте параметры кнопок «поделиться» в социальных сетях Не забудьте включить требуемые кнопки на <a href="@link">странице настроек</a> модуля Если вы включите эту опцию, вы сможете вручную изменять показываемое количество репостов для каждой соцсети <a href="@link">здесь</a>. Вручную установить количество репостов Имя Типы материалов, доступные для шаринга Страница Количество репостов Сохранить Установить тему по умолчанию Настройки Поделиться в Показывать количество репостов на кнопках Простой модуль для вставки темизируемых кнопок социальных сетей в ноды. Интеграция с социальными сетями Настройки кнопок «поделиться» Включить кнопки шаринга в соцсетях Этот модуль разработан именно для возможности изменения внешнего вида кнопок социальных сетей, однако для начала вы можете использовать предустановленную тему. Просмотр и редактирование количества репостов 
<?php

function sharator_get_count($type = NULL, $url = NULL) {
  if ($type)
    switch
    ($type) {
      case 'fb': // Facebook
        $facebook_request = drupal_http_request("http://api.facebook.com/restserver.php?format=json&method=links.getStats&urls=".urlencode($url), array('method' => 'GET'));
        $fb = json_decode($facebook_request->data);
        if (property_exists($fb[0], 'share_count')) {
          return $fb[0]->share_count;
        } else
          return 0;
        break;
      case 'vk': $vk_request = drupal_http_request(
                'http://vk.com/share.php?act=count&index=1&url=' . $url, array('method' => 'GET'));
        $temp = array();
        if (property_exists($vk_request, 'data')) {
          preg_match('/^VK.Share.count\(1, (\d+)\);$/i', $vk_request->data, $temp);
          return $temp[1];
        } else return -1;
        break;
      case 'tw': // Twitter
        $api = drupal_http_request('https://cdn.api.twitter.com/1/urls/count.json?url=' . $url, array('method' => 'GET'));
        if($api->data){
          $count = json_decode($api->data);
          if (property_exists($count, 'count')) {
            return $count->count;
          } else return 0;
        } else return -1;
        break;
      case 'gp': // Google +
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        $curl_results = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($curl_results, true);
        return intval($json[0]['result']['metadata']['globalCounts']['count']);
        break;
      case 'od': // Одноклассники
        $rq = drupal_http_request('http://www.odnoklassniki.ru/dk?st.cmd=extLike&uid=odklcnt0&ref=' . urlencode($url), array('method' => 'GET'));
        if (!$rq->data)
          return 0;
        $res = preg_match("/^ODKL.updateCount\('[\d\w]+','(\d+)'\);$/i", $rq->data, $i) ? (int) $i[1] : 0;
        return $res;
        break;
      case 'mw': // Мой Мир
        $request = drupal_http_request('http://connect.mail.ru/share_count?callback=1&url_list=' . urlencode($url) . '&func=a', array('method' => 'GET'));
        if (!property_exists($request, 'data')) {
          return 0;
        } else {
          return preg_match('{\"shares\":(\d+),\"clicks\":(\d+)}', $request->data, $m) ? (int) $m[1] : 0;
        }
        break;
    }
}

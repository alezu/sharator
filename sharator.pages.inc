<?php

function sharator_config_form($form, &$form_state) {

  $select = db_select('sharator', 's')
          ->fields('s');

  $res = $select->execute();
  $results = $res->fetchAll();

  $config = sharator_get_config();


  $form['sharator_activity'] = array(
      '#type' => 'checkbox',
      '#title' => t('Switch on share buttons'),
      '#default_value' => $config['activity'],
  );

  $node_types = node_type_get_types();
  $options = array();
  foreach ($node_types as $node_key => $node_type) {
    $options[$node_key] = $node_type->name;
  }

  $options_defaults = $config['node_types'];
  if (!is_array($options_defaults))
    $options_defaults = array();
  $form['sharator_node_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Node types available for sharing'),
      '#options' => $options,
      '#default_value' => $options_defaults,
  );

  $form['sharator_show_reposts'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show reposts quantity on share buttons'),
      '#default_value' => $config['show_reposts'],
  );

  $form['sharator_manual_set'] = array(
      '#type' => 'checkbox',
      '#title' => t('Manually set reposts quantity'),
      '#description' => t('If you switch on this checkbox, you will be able to manually change reposts quantity for each available social media <a href="@link">here</a>.', array('@link' => '/admin/config/services/sharator/sharator_nodes')),
      '#default_value' => $config['manual_set'],
  );

  $form['sharator_default_theme'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set default theme'),
      '#description' => t('This module is made especially for customizing social buttons but you can firstly use preset theme'),
      '#default_value' => $config['default_theme'],
  );

  $form['btns_list'] = array(
      '#theme' => 'sharator_form_table',
      '#tree' => TRUE,
  );
  $form['btns_list']['header'] = array(
      '#type' => 'value',
      '#value' => array(t('Name'), t('Activity')
      ),
  );

  foreach ($results as $key => $result) {

    $form['btns_list']['data'][$result->machine_name]['label'] = array(
        '#type' => 'markup',
        '#markup' => $result->name,
    );
    $form['btns_list']['data'][$result->machine_name]['status'] = array(
        '#type' => 'checkbox',
        '#default_value' => $result->status,
        '#value ' => $result->status,
    );
  }

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
  );

  return $form;
}

function sharator_config_form_submit($form, &$form_state) {
  $values = array();
  $default_node_types = array();
  foreach ($form_state['values']['sharator_node_types'] as $node_key => $node_type) {
    if ($node_type) {
      $default_node_types[] = $node_key;
    }
  }

  $configs = array(
      array(
          'setting_name' => 'activity',
          'value' => $form_state['values']['sharator_activity'],
      ),
      array(
          'setting_name' => 'node_types',
          'value' => base64_encode(serialize($default_node_types)),
      ),
      array(
          'setting_name' => 'show_reposts',
          'value' => $form_state['values']['sharator_show_reposts'],
      ),
      array(
          'setting_name' => 'manual_set',
          'value' => $form_state['values']['sharator_manual_set'],
      ),
      array(
          'setting_name' => 'default_theme',
          'value' => $form_state['values']['sharator_default_theme'],
      ),
  );

  foreach ($configs as $record) {
    $query = db_update('sharator_config');
    $query
            ->fields(array('value' => $record['value']))
            ->condition('setting_name', $record['setting_name'])
            ->execute();
  }

  $form_values = $form_state['values']['btns_list']['data'];
  foreach ($form_values as $soc_name => $soc_values) {
    if (is_array($soc_values)) {
      if (isset($soc_values['status'])) {
        $values[$soc_name]['status'] = $soc_values['status'];
      }
      if (isset($soc_values['value'])) {
        $values[$soc_name]['value'] = (int) $soc_values['value'];
      }
    }
  }

  foreach ($values as $key => $record) {
    $query = db_update('sharator');
    $query
            ->condition('machine_name', $key)
            ->fields($record)
            ->execute();
  };

  drupal_set_message(t('Configuration is saved...'));
}

function sharator_list_form($form, &$form_state) {

  $select = db_select('sharator_nodes', 's')
          ->fields('s');
  $res = $select->execute();
  $results = $res->fetchAll();

  $available_media_select = db_select('sharator', 'a')
          ->fields('a', array('machine_name', 'name'));
  $available_media_res = $available_media_select->execute();
  $available_media = $available_media_res->fetchAllKeyed();

  $config = sharator_get_config();

  $form['pages_list'] = array(
      '#theme' => 'sharator_form_table',
      '#tree' => TRUE,
  );
  $form['pages_list']['header'] = array(
      '#type' => 'value',
      '#value' => array(
          array('data' => t('Page')),
      )
  );

  foreach ($available_media as $media) {
    $form['pages_list']['header']['#value'][] = array(
        'data' => $media
    );
  }

  $form['pages_list']['data'] = array();
  if ($config['manual_set']) {
    $field_type = 'textfield';
    $value_type = '#default_value';
  } else {
    $field_type = 'markup';
    $value_type = '#markup';
  }

  foreach ($results as $result) {
    $node = node_load($result->nid);
    $form['pages_list']['data'][$result->nid] = array(
        'label' => array(
            '#type' => 'markup',
            '#markup' => l($node->title, 'node/' . $node->nid, array('attributes' => array('target' => '_blank'))),
        ),
    );

    foreach ($available_media as $key => $media) {
      if(!$config['manual_set'] && $result->$key < 0) $result->$key = 'n/a';
      $form['pages_list']['data'][$result->nid][$key] = array(
          '#type' => $field_type,
          '#size' => 4,
          '#element_validate' => array('element_validate_integer'),
          $value_type => $result->$key,
      );
    }
  }

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
  );
  if(!$config['manual_set']) {
    $form['submit']['#attributes']['disabled'] = 'true';
    $form['submit']['#attributes']['style'] = 'display:none;';
  }

  return $form;
}

function sharator_list_form_submit($form, &$form_state) {

  foreach ($form_state['values']['pages_list']['data'] as $key => $value) {
    $query = db_update('sharator_nodes');
    $query
            ->condition('nid', $key)
            ->fields($value)
            ->execute();
  };

  drupal_set_message(t('Configuration is saved...'));
}